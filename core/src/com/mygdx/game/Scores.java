package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import jdk.internal.misc.JavaLangAccess;
import sun.awt.SoftCache;

import javax.annotation.processing.Filer;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class Scores extends JWindow {
    private Integer currentScore;
    private Integer highestScore;
    private JLabel label;
    private JTextField textField;
    private JLabel label2;
    private JTextField textField2;
    private Map<String, Integer> map;
    private FileWriter fileWriter;
    private Scanner scanner;
    private FileReader fileReader;
    private File file;
    String filePath;
    private String currentLogin;

    public Scores() throws Exception{
        currentLogin = new String();
        filePath = "Scores";
        map = new HashMap<>();
        currentScore = 0;
        highestScore = 0;
        this.setBounds(Constants.SCREEN_WIDTH / 2 + 40, 500, 200, 70);
        label = new JLabel(currentScore.toString());
        textField = new JTextField("CURRENT SCORE:");
        label2 = new JLabel(highestScore.toString());
        textField2 = new JTextField("YOUR HIGHEST SCORE:");

        this.setVisible(false);
        this.getContentPane().setBackground(Color.WHITE);
        this.setLayout(new FlowLayout(FlowLayout.CENTER));
        add(textField);
        add(label);
        add(textField2);
        add(label2);
    }

    public void writeInfoOfGamerToFile() throws IOException {
        if(highestScore < currentScore) {
            highestScore = currentScore;
        }
        map.put(currentLogin, highestScore);
        fileReader.close();
        fileWriter = new FileWriter(file, false);
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            fileWriter.write(entry.getKey() + " " + entry.getValue() + "\n" );
        }
        fileWriter.close();
    }

    public Map getMap(){
        return map;
    }

    public void showMap(){
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.println("login: " + entry.getKey() + " Score = " + entry.getValue());
        }
    }

    void readFromFile() throws IOException {
        try {
            file = new File(filePath);
            //fileWriter = new FileWriter(file, true);
            fileReader = new FileReader(file);
            scanner = new Scanner(file);
            if(!file.exists()){
                file.createNewFile();
            }
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        while(scanner.hasNext()) {
                String login = scanner.next();
                Integer highestScore_ = Integer.parseInt(scanner.next());
            if(login == currentLogin){
                highestScore = highestScore_;
            }
                map.put(login, highestScore_);
        }
        int i = 0;
    }

    public void addLogin(String login){
        if(!map.containsKey(login)){
            map.put(login, highestScore);
        }
        currentLogin = login;
        highestScore = map.get(login);
    }

    public void writeMapToFile() throws IOException {
        for(Map.Entry entry: map.entrySet()) {
            //получить ключ
           // String login = (String) entry.getKey();
            //получить значение
            //Integer highestScore = (Integer) entry.getValue();
            fileWriter.write((String) entry.getKey() + (String) entry.getValue());
        }
    }

    public void closeThreads() throws IOException {
        fileWriter.close();
        fileReader.close();
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public int getHighestScore(){
        return highestScore;
    }

    public void setCurrentScore(int newScore){
        currentScore = newScore;
    }

    public void setHighestScore(String login) {
        for(Map.Entry entry: map.entrySet()) {
            if(entry.getKey() == login){
                highestScore = (Integer) entry.getValue();
            }
        }
    }

    public void setHighestScore(Integer newScore) {
       highestScore = newScore;
    }

    public void update() throws IOException {
        label.setText(currentScore.toString());
        label2.setText(highestScore.toString());
        //readFromFile();
    }

    public void recreate(){
        setVisible(false);
        currentScore = 0;
    }
}