package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import java.io.IOException;

public class MyGdxGame extends ApplicationAdapter {
	private SpriteBatch batch;
	private Background bg;
	private Bird bird;
	private Obstacles obstacles;
	private boolean gameOver;
	private Texture restartTexture;
	private Scores scores;
	private Music music;
	private LoginWindow loginWindow;

	public void start(){
		bird.start();
		obstacles.start();
	}

	@Override
	public void create () {
		try {
			scores = new Scores();
			scores.setVisible(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			scores.readFromFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			loginWindow = new LoginWindow(this, scores);
			loginWindow.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		batch = new SpriteBatch();
		bg = new Background();
		bird = new Bird();
		obstacles = new Obstacles();
		gameOver = false;
		restartTexture = new Texture("restart.png");
		music = Gdx.audio.newMusic(Gdx.files.internal("LHSRLD_10_Chiptune_Keygen_Music.mp3"));
		music.setLooping(true);
		music.setVolume(0.1f);
		music.play();
	}

	@Override
	public void render () {
		try {
			update();//пересчитать математику всей игры
		} catch (IOException e) {
			e.printStackTrace();
		}
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		if((scores.getCurrentScore() / 5) % 2 == 0){
			bg.renderMorning(batch);
		}
		else{
			bg.renderNight(batch);
		}
		obstacles.render(batch);
		if(!gameOver){
			bird.render(batch);
		}else{
			batch.draw(restartTexture, (Constants.SCREEN_WIDTH - restartTexture.getWidth()) / 2, (Constants.SCREEN_HEIGHT - restartTexture.getHeight()) / 2);
		}
		batch.end();
//		music.setOnCompletionListener(new Music.OnCompletionListener() {
//			@Override
//			public void onCompletion(Music aMusic) {
//				music.stop();
//				musicNight.play();
//			}
//		});
	}

	public void update() throws IOException {
		bg.update();
		bird.update();
		obstacles.update();
		scores.update();
		for(int i = 0; i < Obstacles.obs.length; i++){
			if(bird.getPosition().x > Obstacles.obs[i].getPosition().x && bird.getPosition().x < Obstacles.obs[i].getPosition().x + obstacles.getTexture().getWidth()){
				Vector2 TopBirdPosition = new Vector2(bird.getPosition().x, bird.getPosition().y + bird.getImg().getHeight());
				if(!Obstacles.obs[i].getEmptySpace().contains(bird.getPosition())||!Obstacles.obs[i].getEmptySpace().contains(TopBirdPosition)) {
					gameOver = true;
				}
			}
			if( (Obstacles.obs[i].getPosition().x == (bird.getPosition().x - bird.getImg().getWidth())) && !gameOver){
				scores.setCurrentScore(scores.getCurrentScore() + 1);
			}
		}

		if(bird.getPosition().y < 0 || bird.getPosition().y > Constants.SCREEN_HEIGHT){
			gameOver = true;
			scores.setVisible(true);
			scores.writeInfoOfGamerToFile();
			//score.showScore();
		}
		if(Gdx.input.isKeyPressed(Input.Keys.SPACE) && gameOver){
			recreate();
			scores.setVisible(false);
		}
	}//просчет математики игры

	@Override
	public void dispose () {
		batch.dispose();
	}

	public void recreate() throws IOException {
		bird.recreate();
		obstacles.recreate();
		gameOver = false;
		if(scores.getHighestScore() < scores.getCurrentScore()){
			scores.setHighestScore(scores.getCurrentScore());
		}
		//scores.readFromFile();
		scores.recreate();
		//scores.writeInfoOfGamerToFile();
	}
}

