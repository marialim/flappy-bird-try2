package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class Bird {
    private Texture img;
    private Vector2 position;
    private float vy;
    private float gravity;//ускорение
    private boolean act;

    public Vector2 getPosition(){
        return position;
    }

    public Texture getImg(){
        return img;
    }

    public Bird(){
        act = false;
        img = new Texture("bird.png");
        position= new Vector2(200, 400);
        vy = 0;
        gravity = -0.7f;
    }

    public void start(){
        act = true;
    }


    public void render(SpriteBatch batch){//с помощью батча отрисовываются все объекты
        batch.draw(img, position.x, position.y);
    }

    public void update(){
        if(act)
        {
            if(Gdx.input.isKeyPressed(Input.Keys.SPACE)){
                vy = 10;//изменить на 180 град положение объекта
            }
            vy += gravity;
            position.y += vy;
        }
    }
    public void recreate(){
        position = new Vector2(200, 400);
        vy = 0;
    }
}

