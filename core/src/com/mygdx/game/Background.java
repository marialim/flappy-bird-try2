package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.sun.org.apache.bcel.internal.Const;

public class Background {

    class BGPicture{
        private Texture night;
        private Texture morning;
        private Texture evening;
        private Vector2 pos;
        public BGPicture(Vector2 pos){
            night = new Texture("evening.png");
           // evening = new Texture("evening.png");
            morning = new Texture("morning.png");
            this.pos = pos;
        }
    }

    private int speed;
    private BGPicture[] backs;

    public Background(){
        speed = 4;//4 точки
        backs = new BGPicture[2];
        backs[0] = new BGPicture(new Vector2(0, 0));
        backs[1] = new BGPicture(new Vector2(Constants.SCREEN_WIDTH, 0));
    }

    public void renderNight(SpriteBatch batch){//отрисовки фона с помощью батча
        for(int i = 0; i < backs.length; i++){
            batch.draw(backs[i].night, backs[i].pos.x, backs[i].pos.y);
        }
    }

    public void renderEvening(SpriteBatch batch){//отрисовки фона с помощью батча
        for(int i = 0; i < backs.length; i++){
            batch.draw(backs[i].evening, backs[i].pos.x, backs[i].pos.y);
        }
    }

    public void renderMorning(SpriteBatch batch){//отрисовки фона с помощью батча
        for(int i = 0; i < backs.length; i++){
            batch.draw(backs[i].morning, backs[i].pos.x, backs[i].pos.y);
        }
    }

    public void update(){
        for(int i = 0; i < backs.length; i++){
            backs[i].pos.x -= speed;
        }
        if(backs[0].pos.x < -Constants.SCREEN_WIDTH){
            backs[0].pos.x = 0;
            backs[1].pos.x = Constants.SCREEN_WIDTH;
        }
    }
}
