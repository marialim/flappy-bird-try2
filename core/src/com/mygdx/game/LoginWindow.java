package com.mygdx.game;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class LoginWindow extends JFrame {
    private Scores scores;
    private String login;
    private JTextField textLogin;
    private JLabel labelForLogin;
    private Button button;
    private MyGdxGame mgd;
    public LoginWindow(MyGdxGame myGdxGame, Scores scores_) throws Exception {
    //scores = scores_;
        scores = scores_;
        mgd = myGdxGame;
        labelForLogin = new JLabel("Enter login: ");
        textLogin = new JTextField( 10);
        button = new Button("Enter");
        this.setBounds(Constants.SCREEN_WIDTH / 2 + 40, 500, 200, 150);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new FlowLayout());
        this.add(labelForLogin);
        this.add(textLogin);
        button.addActionListener(new LoginWindow.ButtonEventListener());
        //работаем с кнопкой. AddActionListener вызывает класс, в котором будет реализация действия
        this.add(button);
        System.out.println(mgd);
    }

    class ButtonEventListener implements ActionListener {//реализует интерфейс
        public ButtonEventListener(){
        }
        public void actionPerformed(ActionEvent e) {//какое-то событие передается в качестве параметра
            setVisible(false);
            mgd.start();
            login = textLogin.getText();
            scores.addLogin(login);
            scores.setHighestScore(login);
            scores.showMap();
            try {
                scores.writeInfoOfGamerToFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}