package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Constants;

import java.util.Random;

public class Obstacles {
    class WallPair{
        private Vector2 position;
        private float speed;
        private int offset;
        private Rectangle emptySpace;//пустое расстояние между труб
        private boolean act;

        private WallPair(Vector2 pos){
            position = pos;
            speed = 2;
            offset = new Random().nextInt(300);
            emptySpace = new Rectangle(position.x, position.y - offset + txt.getHeight(), txt.getWidth(), betweenDistance);
            act = false;
        }

        public void start(){
            act = true;
        }

        public Vector2 getPosition(){
            return position;
        }

        public Rectangle getEmptySpace(){
            return emptySpace;
        }

        public void update(){
            if(act){
                position.x -= speed;
                if(position.x < -txt.getWidth()){
                    position.x = Constants.SCREEN_WIDTH;
                    offset = new Random().nextInt(300);
                    emptySpace = new Rectangle(position.x, position.y - offset + txt.getHeight(), txt.getWidth(), betweenDistance);
                }
                emptySpace.x = position.x;
            }
        }
    }
    public void start(){
        for(WallPair obstacle : obs){
            obstacle.start();
        }
    }

    public static WallPair[] obs;
    private Texture txt;
    private int betweenDistance;

    public Texture getTexture(){
        return txt;
    }

    public Obstacles(){
        txt = new Texture("wall2.png");
        obs = new WallPair[6];
        betweenDistance = 300;
        int startPosX = 500;
        for(int i = 0; i < obs.length; i++){
            obs[i] = new WallPair(new Vector2(startPosX, 0));
            startPosX += 220;
        }
    }
    public void render(SpriteBatch batch){
        for(int i = 0; i < obs.length; i++){
            batch.draw(txt, obs[i].position.x, obs[i].position.y - obs[i].offset);//юзать emptyspace
            batch.draw(txt, obs[i].position.x, obs[i].position.y + obs[i].emptySpace.height + txt.getHeight() - obs[i].offset);
        }
    }
    public void update(){
        for(WallPair obstacle : obs) {
            obstacle.update();
        }
    }

    public void recreate(){

        int startPosX = 500;
        for(int i = 0; i < obs.length; i++){
            obs[i] = new WallPair(new Vector2(startPosX, 0));
            obs[i].start();
            startPosX += 220;
        }
    }
}//класс для всех пар труб
